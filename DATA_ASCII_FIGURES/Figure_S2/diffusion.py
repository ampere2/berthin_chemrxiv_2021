#!usr/bin/python
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
from math import pi

x = np.array([0.0179, 0.0202, 0.0243, 0.0285, 0.0377])          # 1/AA
y = np.array([3.5469, 3.5175, 3.4470, 3.3836, 3.2093])      # 10-9 m2/s
y_err = np.array([0.00880, 0.00686, 0.02264, 0.02144, 0.03762])

fig=plt.figure(figsize=(6.5,5))
name_file_save=r'diffusion_acn'
name_file_save+='.pdf'
ax = fig.add_subplot(1,1,1)

(slope, intercept, r_value, p_value, std_err) = linregress(x, y)

plt.title(r'Diffusion coefficient', fontsize = 16) 
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
ax.tick_params(axis='both', width=1.5)
ax.spines['left'].set_linewidth(1.5)
ax.spines['bottom'].set_linewidth(1.5)
ax.spines['right'].set_linewidth(1.5)
ax.spines['top'].set_linewidth(1.5)

visco = -(2.837*1.38064852e-23*298)/(6*pi*slope*1e-19*1e-03)
ax.set_xlim(0.015,0.0401)
start, end = ax.get_xlim()
ax.xaxis.set_ticks(np.arange(start, end, 0.005))

plt.plot(x, y, '+', color='mediumvioletred')
plt.plot(x, intercept + slope*x, 'midnightblue', linewidth = 1.5)
plt.errorbar(x, y, yerr = y_err, fmt = 'none', capsize = 10, ecolor = 'mediumvioletred', zorder = 1)
plt.tick_params(axis = 'both', labelsize = 16)

plt.text(0.029, 3.5, r'D$_{0}$ = '+str(round(intercept,2)*1e-09)+r' m$^{2}$.s$^{-1}$'+'\n'+r'$\eta$ = '+str(round(visco,3))+' mPa.s', fontsize = 14)
plt.xlabel(r'L$^{-1}$ ($\mathrm{\AA}$)', fontsize = 14)
plt.ylabel(r'D$_{PBC}$ ($\times$ 10$^{-9} $m$^{2}$.s$^{-1}$)', fontsize = 14)
fig.savefig(name_file_save, bbox_inches='tight')
