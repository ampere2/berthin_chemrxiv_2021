# Berthin_JChemPhys_155_074504_2021

Contains input files and data used to generate the figures of the article:

Solvation of anthraquinone and Tempo redox-active species in acetonitrile using a polarizable force field

Roxanne Berthin, Alessandra Serva, Kyle Reeves, Esther Heid, Christian Schroeder, and Mathieu Salanne,
*J. Chem. Phys.*, 155, 074504, 2021

https://doi.org/10.1063/5.0061891 ([see here for a preprint](https://chemrxiv.org/engage/chemrxiv/article-details/60db063d01b3e07223ea9ea9))

The folder *InputFiles* contains input files for each system for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The folders *FigureN* where N goes from 2 to 7 contain files with the data used to plot the Figures of the paper 
The folders *FigureSN* where N goes from 1 to 4 contain files with the data used to plot the Supplementary Figures of the paper 
